## ----setup, include=FALSE------------------------------------------------
knitr::opts_chunk$set(echo = TRUE)

## ----install, eval=FALSE-------------------------------------------------
#  install.packages(devtools)
#  devtools::install_git('https://gitlab.com/belell/CDMquery.git')

## ----load----------------------------------------------------------------
library(CDMquery)

## ----createstudy101, echo=FALSE, results='hide'--------------------------
unlink("study_101", recursive = TRUE) 
dir.create("study_101")
file.copy(system.file("extdata", "screening.txt", package = "CDMquery"), "study_101", overwrite = TRUE)

## ----firstlist-----------------------------------------------------------
list.files("study_101")

## ----init----------------------------------------------------------------
init("study_101")

## ----secondlist----------------------------------------------------------
list.files("study_101")

## ----thirdlist-----------------------------------------------------------
list.files("study_101/queries")

## ----fourthlist----------------------------------------------------------
list.files("study_101/queries/definitions")

## ----create_qdef, echo=FALSE, results='hide'-----------------------------
file.copy(system.file("extdata", "querydefinitions.txt", package = "CDMquery"), "study_101/queries/definitions/querydefinitions.txt", overwrite = TRUE)

## ----init2---------------------------------------------------------------
init("study_101")

## ----show data, echo=FALSE-----------------------------------------------
knitr::kable(read.delim("study_101/screening.txt"), caption = "The data file ('screening.txt')" )

## ----showdefs, echo=FALSE------------------------------------------------
file_qdef <- "study_101/queries/definitions/querydefinitions.txt"
knitr::kable(read.delim(file_qdef), caption = "Query definitions" )

## ----queryone, eval=TRUE-------------------------------------------------
query("study_101", "screening.txt", "tsv", c("subjectID", "initials"))

## ------------------------------------------------------------------------
list.files("study_101/queries/findings")

## ---- showcurrent_2, eval=TRUE, echo=FALSE-------------------------------
findcurr <- "study_101/queries/findings/queryfindings_current.txt"
knitr::kable(read.delim(findcurr), caption = "Current findings in the table 'currrentfindings.txt'")

## ----copyscreen_2, echo=FALSE, results='hide', eval=FALSE----------------
#  file.copy(system.file("extdata", "screening_2.csv", package = "CDMquery"), "study_101/screening.txt", overwrite = TRUE)

## ----update_scr_1, echo=FALSE, results='markup'--------------------------
scr <- read.delim("study_101/screening.txt")
scr[scr$subjectID == "L-003", "bp_syst"] <- 120
scr[scr$subjectID == "L-005", "sex"] <- "M"
write.table(scr, "study_101/screening.txt", sep = "\t", row.names = FALSE)
knitr::kable(read.delim("study_101/screening.txt"), caption = "New 'screening.csv' table after 2 corrections")

## ----copycurr_2, echo=FALSE, results='markup'----------------------------
curr <- read.delim(findcurr, stringsAsFactors = FALSE)
curr$action[curr$subjectID == "L-005" & curr$var1 == "sex"] <- "corrected"
curr$action[curr$subjectID == "L-005" & curr$var1 == "ageyr"] <- "confirmed"
curr$action[curr$subjectID == "L-003"] <- "corrected"

write.table(curr, findcurr, sep = "\t", row.names = FALSE)
knitr::kable(read.delim("study_101/queries/findings/queryfindings_current.txt"))

## ----querytwo, eval = TRUE-----------------------------------------------
query("study_101", "screening.txt", "tsv", c("subjectID", "initials"))

## ----showconf, eval=TRUE, echo=FALSE-------------------------------------
knitr::kable(read.delim("study_101/queries/confirmed/queryconfirmed.txt"))

## ----copycurr_3, echo=FALSE----------------------------------------------
curr <- read.delim(findcurr)
curr$action <- "confirmed"
write.table(curr, findcurr, sep = "\t", row.names = FALSE)
knitr::kable(read.delim("study_101/queries/findings/queryfindings_current.txt"))

## ----querythree, eval = TRUE---------------------------------------------
query("study_101", "screening.txt", "tsv", c("subjectID", "initials"))

## ----showconf_arch, echo=FALSE-------------------------------------------
knitr::kable(read.delim("study_101/queries/confirmed/queryconfirmed.txt"), caption = "confirmed_archive table")

## ----showfind_arch, echo=FALSE-------------------------------------------
knitr::kable(read.delim("study_101/queries/findings/queryfindings_archive.txt", header = TRUE), caption = "findings_archive table")

## ----copylib, echo=FALSE, results='hide'---------------------------------
file_lib <- system.file("extdata", "querydefinitions_lib.txt", package = "CDMquery")


## ----showlibrary, echo=FALSE, results='markup'---------------------------
knitr::kable(read.delim("study_101/queries/library/querydefinitions_lib.txt"), caption = "query definitions library")

