#' Detect inconsistencies in data and store findings
#'
#' @param projectfolder  A character string naming the project file. The \file{queries} folder and sub folders are created in this folder.
#'   The output of queries are stored in this folder and its sub-folders.
#'     Putting the data file in this folder
#' @param file A character string naming the file containing the data to query
#' @param source A character string indicating the
#' @param uniqueID A vector of unique IDs to be displayed in output
#' @param return logic variable indicating whether the current findings should be displayed
#'
#' @return Data.frame containing the result of the queries.
#' @export query
#' @import dplyr stringr knitr
#' @importFrom utils read.csv write.csv read.delim
#' @importFrom stats na.omit
#'
#' @examples
#' \dontrun{
#' query("projectfolder", file = "dummydata.csv", source = "csv",
#' c("subjectID", "initials"), return = TRUE )
#' }
query <- function(projectfolder, file, source = c("tsv", "csv"), uniqueID, return = TRUE){

  # 1. Checking  folder structure------------------------------------------------------------------------

  if(!dir.exists(projectfolder)){
    stop("Project folder not found. Have you initialised the workspace with 'initialise()' ?")
  }

  # 2.  Import data-----------------------------------------------------------------------

  file2 <- paste0(projectfolder, "/", file) # change: if file contains "/" use file else projfolde
  def_file <- paste0(projectfolder, "/queries/definitions/querydefinitions.csv")

  if(source == "csv") dtb <- read.csv(file2, stringsAsFactors = FALSE)
  else if(source == "tsv") dtb <- read.delim(file2, stringsAsFactors = FALSE)

  reqt <-
    read.csv(def_file, stringsAsFactors = FALSE) %>%
    mutate(expression = as.character(expression),
           var1 = as.character(var1),
           var2 = as.character(var2))

  vs <- c(reqt$var1, na.omit(reqt$var2))
  bn <- vs[!vs %in% names(dtb)]
  if(length(bn) != 0) {
    cat("The following variables in qdef do not exist in the data table:\n")
    cat(bn)
    stop("please check the variables of the query definitions file")
  }

  # 3. Performing queries-----------------------------------------------------------------------

  curr_find <- data.frame() # name: curr_find
  for(i in 1:nrow(reqt)){
    par1 <- reqt[i,"var1"]
    par1 <- as.character(par1)
    var1 <- dtb[, par1]
    par2 <- reqt[i,"var2"]
    par2 <- as.character(par2)

    if(!is.na(par2)){
      var2 <- dtb[,par2]
    }
    q <- eval(parse(text = reqt[i, "expression"]))
    q[is.na(q)] <- FALSE

    if (sum(q) != 0) {
      if(!is.na(par2)){
        value2 <- as.character(dtb[q, par2])
      } else value2 <- "NA"

      out <- cbind(dtb[q, uniqueID], value1 = as.character(dtb[q, par1]),
                   value2, runtime = format(Sys.time(), "%Y-%m-%d %H:%M"))
      tog <- merge(out, reqt[i, c("var1","var2", "output")])
#      tog <- tog[, c(5,1,2,6,3,7,4,8)]
      tog <- tog[, c("runtime", uniqueID, "var1", "value1", "var2", "value2","output")]
      curr_find <- rbind(curr_find, tog)
    }
  }

  action <- NULL
  curr_find$action <- "confirmed or corrected"


# store current findings --------------------------------------------------
  filecurrent <- paste0(projectfolder, "/queries/findings/queryfindings_current.csv")

  if(file.exists(filecurrent)) {
    prev_find <- read.csv(filecurrent, stringsAsFactors = FALSE)
  } else {
    prev_find <- curr_find
  }

  write.csv(curr_find, file = filecurrent, row.names = FALSE)

# 4. archive -----------------------------------------------------------------
# current findings already in archive should not be added to the archive.
# findings for which the uniqueIDs and the output are identical are not added

  filearchive <- paste0(projectfolder, "/queries/findings/queryfindings_archive.csv")

  if(!file.exists(filearchive)) write.csv(curr_find, file = filearchive, row.names = FALSE)
  qarchive <- read.csv(filearchive, stringsAsFactors = FALSE)
  qarchive <- rbind(qarchive, curr_find)
  qarchive_new <- qarchive[!duplicated(qarchive[, c(uniqueID, "output")]),]

  write.csv(qarchive_new , file = filearchive, row.names = FALSE )

# 5. confirmed ------------------------------------------------------------
#browser()
  fileconfirmed <- paste0(projectfolder, "/queries/confirmed/queryconfirmed.csv")

  if (file.exists(fileconfirmed)){
    prev_conf <- read.csv(fileconfirmed, stringsAsFactors = FALSE)
    prev_conf <- prev_conf[,-1]
  } else prev_conf <- data.frame()

  curr_conf <- prev_find[prev_find$action == "confirmed", ]

  if(nrow(curr_conf) != 0){
    arch_conf <- rbind(curr_conf, prev_conf)
    write.csv(arch_conf, file = fileconfirmed, row.names = FALSE)

# only use current findings that were not confirmed
# cat("curr_find\n")
# str(curr_find)
#
# cat("curr_conf\n")
# str(curr_conf)

    curr_find <-
      curr_find %>%
      anti_join(curr_conf, by = c(uniqueID, "output"))

#print(curr_find2)

    # curr_find <-
    #   prev_find %>%
    #   semi_join(curr_find, by = "action")

    write.csv(curr_find, file = filecurrent, row.names = FALSE)
  }

#  setwd(wd)
  if(return == TRUE) return(curr_find[, -c(1,length(curr_find))])
}


#

#
# #4.  Queries confirmed deleted--------------------------------------------------------------
#
# fileconfirmed <- "queries/confirmed/queryconfirmed.csv" # the path where  queries
#
# if (file.exists("queries/confirmed/queryconfirmed.csv")){
#   confKeep <- read.csv(fileconfirmed)
#   confKeep <- confKeep[,-1]
#
# } else confKeep <- data.frame()
#
#
# conf <- prev_find[prev_find$action == "confirmed", ]
#
# # To keep all confirmed queries only when there is queries confirmed
#
#
# if(nrow(conf) != 0){
#   write.csv(conf, file = fileconfirmed, row.names = FALSE)
#
#   #conf$time <- format(Sys.time())
#
#   confKeep <- rbind(conf, confKeep)
#
#   write.csv(confKeep, file = fileconfirmed, row.names = FALSE)
#
#
#   # To delete queries confirmed by user
#
#   prev_find <-
#     prev_find %>%
#     semi_join(curr_find, by = "action")
#
#   write.csv(prev_find, file = filecurrent, row.names = FALSE)
#
#
# } #else  write.csv(confKeep, file = fname3)
