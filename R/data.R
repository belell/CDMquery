# put in r/data , add name of data

#' Villages in Moyen-Ogooue
#'
#' Dataframe for villages
#' @name villages
#' @field nom Contain the name of some villages in Moyen-Ogooué.
#' @field Lambarene variable which indicates wether  the variable is in Lambarene
#' @docType data
#' @keywords data
"villages"
