
#' Sets up the workspace by adding folders and templates to the project folder
#'
#' @param projectfolder  A vector
#'
#' @return after
#' @export initialise
#' @import dplyr stringr
#'
#' @examples
#' \dontrun{
#' initialise("TB_lab")
#' }

initialise <- function(projectfolder){

  if(!dir.exists(projectfolder)){
    stop("Project folder not found")
  }
  wd <- getwd()
  setwd(projectfolder) #Please can you use this line when
                       #we are sure that projectfolder were created
                       #or exists.

  fn <- c("queries", "queries/definitions", "queries/findings",
          "queries/confirmed", "queries/externaldata", "queries/library")

  for(i in fn) {
    if(!dir.exists(i)) {
      dir.create(i)
      cat("creating folder", i, "\n")
    }
  }

  if(!file.exists("Queries/Definitions/queryDefinitions.csv")){
    def_template <- data.frame(nr = character(), queryclass = character(), var1 = character(), var2 = character(),
                               expression = character(), output = character())
    write.csv(def_template, file = "Queries/Definitions/queryDefinitions.csv", row.names = FALSE)
    cat("adding 'querydefinitions' template to 'definitions' folder \n")
  }

  # if(!file.exists("Queries/Definitions/parameters.txt")){
  #   param_template <-
  #     data.frame(parameter = c("project folder", "source", "file", "uniqueID"),
  #                value = c(projectfolder, "","",""))
  #   write.table(param_template, file = "Queries/Definitions/parameters.txt", sep = "\t", row.names = FALSE)
  #   cat("adding 'parameters' template to 'definitions' folder\n")
  # }

  ex <-TRUE

  qdef <- read.csv("Queries/Definitions/queryDefinitions.csv")
  if(nrow(qdef) == 0) {
    cat("\n The queryDefinitions file is empty. You need to add it in the 'definitions' folder\n")
    ex <- FALSE
  } else {
    nas <- sapply(qdef[, -4], function(x) any(is.na(x)))
    nanas <- names(nas[nas])
    if(length(nanas) > 0) {
      cat("\n The following columns in the 'queryDefinitions.csv' file have missing values:\n", nanas)
      ex <- FALSE
    }
  }

  # para <- read.delim("Queries/Definitions/parameters.txt", sep = "\t")
  # mp <- as.character(para[para$value == "", 1])
  #
  # if(length(mp) != 0) {
  #   cat("\n The 'parameters' file is not complete. The following rows have missing values:\n", mp)
  #   ex <- FALSE
  # }

  if(ex) cat("\n The working space has been initialised successfully.")
  setwd(wd)
  return(ex)# function should have a return value.
}
