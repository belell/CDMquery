#' Initialises the workspace by adding folders and templates to the project folder
#'
#' @param projectfolder A character string naming the file to write to.
#'   The \file{queries} folder and sub-folders are created in this folder.
#'   The absolute path must be given, (e.g. 'C:/User/sau/documents/etc') using
#'   forward slashes for Windows.
#'
#' @return nothing
#' @export init
#' @import dplyr
#'
#' @examples
#' \dontrun{
#' init("C:/Users/sau/Documents/study_101")
#' }

init <- function(projectfolder){

  if(!dir.exists(projectfolder)) stop("Project folder not found")

  fn <- c("/queries", "/queries/definitions", "/queries/findings",
          "/queries/confirmed", "/queries/externaldata", "/queries/library")

  fn2 <- paste0(projectfolder, fn)

  for(i in fn2) {
    if(!dir.exists(i)) {
      dir.create(i)
      cat("creating folder", i, "\n")
    }
  }

  file_qdef <- paste0(projectfolder, "/queries/definitions/querydefinitions.txt")
  ext_qdef <- system.file("extdata", "querydefinitions_tmpt.txt", package = "CDMquery")
  if(!file.exists(file_qdef)){
    file.copy(ext_qdef, file_qdef, overwrite = TRUE)
    cat("\nadding 'querydefinitions' template to 'definitions' folder\n")
  }

  file_qdeflib <- paste0(projectfolder, "/queries/library/querydefinitions_lib.txt")
  ext_qdeflib <- system.file("extdata", "querydefinitions_lib.txt", package = "CDMquery")
  if(!file.exists(file_qdeflib)){
    file.copy(ext_qdeflib, file_qdeflib, overwrite = TRUE)
    cat("adding 'querydefinitions' library to 'library' folder\n")
  }

  file.copy(ext_qdeflib, file_qdeflib, overwrite = TRUE)

  ex <-TRUE

  qdef <- read.delim(file_qdef)
  if(nrow(qdef) == 0) {
    cat("\nNOTE: The queryDefinitions file is empty.  You need to add it in the 'definitions' folder\n")
    ex <- FALSE
  } else {
    nas <- sapply(qdef[, -4], function(x) any(is.na(x)))
    nanas <- names(nas[nas])
    if(length(nanas) > 0) {
      cat("\n The following columns in the 'queryDefinitions.csv' file have missing values:\n", nanas)
      ex <- FALSE
    }
  }

  if(ex) cat("The working space has been initialised successfully.\n")
}
